Instructions for testing
========================
Run in eclipse as a test uni unit

Test unit location
/maven-plugin/src/test/java/com/yoshtec/owl/jcodegen/GenCode.java

Test data locations
	OWL ontology in
		/maven-plugin/src/test/resources/usecase.owl
	Generated java class source files in
		/maven-plugin/target/otest/usecase

