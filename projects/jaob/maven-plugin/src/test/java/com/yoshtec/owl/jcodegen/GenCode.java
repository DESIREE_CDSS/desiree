package com.yoshtec.owl.jcodegen;

import java.io.File;

import org.junit.Ignore;
import org.junit.Test;

public class GenCode {
	//	 TODO : transform to main()
	private static final File JAVA_SOURCE_FOLDER = new File("target/otest");
//	private static final File JAVA_SOURCE_FOLDER = new File("/media/sf_share/Food/DESIREE/ontos/usecase/java");
	
	@Test
	public void testCodegenTestCase() throws Exception {
		Codegen codegen = new Codegen();
		codegen.setJavaPackageName("usecase");
		
//		codegen.setOntologyIri("http://www.co-ode.org/ontologies/testcase/testcase.owl");
		codegen.setOntologyIri("http://www.desiree.org/ontologies/usecase");
		codegen.setOntologyPhysicalIri(new File("src/test/resources/usecase.owl").toURI().toString());
		codegen.setJavaSourceFolder(JAVA_SOURCE_FOLDER);
		codegen.setGenerateIdField(true);
		codegen.setGenerateInterfaces(true);	// Just a Try
		codegen.setShortcutSeparator("_");
		codegen.genCode();
	}
}
