Instructions for testing
========================

This project is compiling a utility and building it as owl2xsd.jar
Given a owl ontology as input , it computes a corresponding xsd schema.
This generation exploits the Domain && Range properties for the concepts used in the ontology to define the types of the xsd fields of the objects.
The "functional" property of the data && objects properties is used to determine the cardinality of the object fields.

The produced xsd schema have to be parsed later using xjc to produce java classes package.
Run the example in  example/make_cgo.sh

