//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2016.12.01 à 12:29:43 PM CET 
//


package desiree.cgo.java;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour CGDVar complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CGDVar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NValue" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BValue" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="hasTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Tvalue" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="hasType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hasCode" type="{}Coding"/>
 *         &lt;element name="hasRange" type="{}VarDomain"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CGDVar", propOrder = {
    "nValue",
    "sValue",
    "bValue",
    "hasTime",
    "tvalue",
    "hasType",
    "hasCode",
    "hasRange"
})
@XmlSeeAlso({
    EHRVar.class,
    DecisionVar.class
})
public class CGDVar {

    @XmlElement(name = "NValue")
    protected int nValue;
    @XmlElement(name = "SValue", required = true)
    protected java.lang.String sValue;
    @XmlElement(name = "BValue")
    protected boolean bValue;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar hasTime;
    @XmlElement(name = "Tvalue", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar tvalue;
    @XmlElement(required = true)
    protected java.lang.String hasType;
    @XmlElement(required = true)
    protected Coding hasCode;
    @XmlElement(required = true)
    protected VarDomain hasRange;

    /**
     * Obtient la valeur de la propriété nValue.
     * 
     */
    public int getNValue() {
        return nValue;
    }

    /**
     * Définit la valeur de la propriété nValue.
     * 
     */
    public void setNValue(int value) {
        this.nValue = value;
    }

    /**
     * Obtient la valeur de la propriété sValue.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.String }
     *     
     */
    public java.lang.String getSValue() {
        return sValue;
    }

    /**
     * Définit la valeur de la propriété sValue.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.String }
     *     
     */
    public void setSValue(java.lang.String value) {
        this.sValue = value;
    }

    /**
     * Obtient la valeur de la propriété bValue.
     * 
     */
    public boolean isBValue() {
        return bValue;
    }

    /**
     * Définit la valeur de la propriété bValue.
     * 
     */
    public void setBValue(boolean value) {
        this.bValue = value;
    }

    /**
     * Obtient la valeur de la propriété hasTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHasTime() {
        return hasTime;
    }

    /**
     * Définit la valeur de la propriété hasTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHasTime(XMLGregorianCalendar value) {
        this.hasTime = value;
    }

    /**
     * Obtient la valeur de la propriété tvalue.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTvalue() {
        return tvalue;
    }

    /**
     * Définit la valeur de la propriété tvalue.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTvalue(XMLGregorianCalendar value) {
        this.tvalue = value;
    }

    /**
     * Obtient la valeur de la propriété hasType.
     * 
     * @return
     *     possible object is
     *     {@link java.lang.String }
     *     
     */
    public java.lang.String getHasType() {
        return hasType;
    }

    /**
     * Définit la valeur de la propriété hasType.
     * 
     * @param value
     *     allowed object is
     *     {@link java.lang.String }
     *     
     */
    public void setHasType(java.lang.String value) {
        this.hasType = value;
    }

    /**
     * Obtient la valeur de la propriété hasCode.
     * 
     * @return
     *     possible object is
     *     {@link Coding }
     *     
     */
    public Coding getHasCode() {
        return hasCode;
    }

    /**
     * Définit la valeur de la propriété hasCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Coding }
     *     
     */
    public void setHasCode(Coding value) {
        this.hasCode = value;
    }

    /**
     * Obtient la valeur de la propriété hasRange.
     * 
     * @return
     *     possible object is
     *     {@link VarDomain }
     *     
     */
    public VarDomain getHasRange() {
        return hasRange;
    }

    /**
     * Définit la valeur de la propriété hasRange.
     * 
     * @param value
     *     allowed object is
     *     {@link VarDomain }
     *     
     */
    public void setHasRange(VarDomain value) {
        this.hasRange = value;
    }

}
