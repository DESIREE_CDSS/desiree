//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2016.12.01 à 12:29:43 PM CET 
//


package desiree.cgo.java;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the desiree.cgo.java package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: desiree.cgo.java
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancerType }
     * 
     */
    public CancerType createCancerType() {
        return new CancerType();
    }

    /**
     * Create an instance of {@link PR }
     * 
     */
    public PR createPR() {
        return new PR();
    }

    /**
     * Create an instance of {@link EHRVar }
     * 
     */
    public EHRVar createEHRVar() {
        return new EHRVar();
    }

    /**
     * Create an instance of {@link VarDomain }
     * 
     */
    public VarDomain createVarDomain() {
        return new VarDomain();
    }

    /**
     * Create an instance of {@link Patient }
     * 
     */
    public Patient createPatient() {
        return new Patient();
    }

    /**
     * Create an instance of {@link String }
     * 
     */
    public String createString() {
        return new String();
    }

    /**
     * Create an instance of {@link ER }
     * 
     */
    public ER createER() {
        return new ER();
    }

    /**
     * Create an instance of {@link Recommendation }
     * 
     */
    public Recommendation createRecommendation() {
        return new Recommendation();
    }

    /**
     * Create an instance of {@link Coding }
     * 
     */
    public Coding createCoding() {
        return new Coding();
    }

    /**
     * Create an instance of {@link TumorPR }
     * 
     */
    public TumorPR createTumorPR() {
        return new TumorPR();
    }

    /**
     * Create an instance of {@link Tumor }
     * 
     */
    public Tumor createTumor() {
        return new Tumor();
    }

    /**
     * Create an instance of {@link CGDVar }
     * 
     */
    public CGDVar createCGDVar() {
        return new CGDVar();
    }

    /**
     * Create an instance of {@link TumorER }
     * 
     */
    public TumorER createTumorER() {
        return new TumorER();
    }

    /**
     * Create an instance of {@link TumorHER2 }
     * 
     */
    public TumorHER2 createTumorHER2() {
        return new TumorHER2();
    }

    /**
     * Create an instance of {@link TumorStatus }
     * 
     */
    public TumorStatus createTumorStatus() {
        return new TumorStatus();
    }

    /**
     * Create an instance of {@link HER2 }
     * 
     */
    public HER2 createHER2() {
        return new HER2();
    }

    /**
     * Create an instance of {@link ComplexType }
     * 
     */
    public ComplexType createComplexType() {
        return new ComplexType();
    }

    /**
     * Create an instance of {@link BodySite }
     * 
     */
    public BodySite createBodySite() {
        return new BodySite();
    }

    /**
     * Create an instance of {@link DecisionVar }
     * 
     */
    public DecisionVar createDecisionVar() {
        return new DecisionVar();
    }

}
