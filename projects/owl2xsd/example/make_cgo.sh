#!/bin/bash
#
#
#
#	Extract a XSD schema cgo.xsd from owl ontology cgo.owl
#
java -jar owl2xsd.jar cgo.owl

#
#	Generate java classes for package desiree.cgo.java using xjc
#	in directory java_src
#	from WSD schema cgo.xsd
xjc -d java_src -p desiree.cgo.java cgo.xsd

