package desiree.org.owl2xsd;

import java.io.File;
import java.io.PrintWriter;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;


public class App 
{
	private static OWLOntologyManager manager;
	
    public static void main( String[] args )
    {
		String inputFilename = args[0];
		String outputFilename = suffixChange( inputFilename, "xsd");	// Default
		if ( args.length > 1 ){
			outputFilename = args[1];
		}
        File outFile = new File(outputFilename);     

		try {
			PrintWriter writer = new PrintWriter(outFile);
			OWLOntology o = shouldLoad(inputFilename);
			XSDDumper dumper = new XSDDumper();
			dumper.dump(o, writer);
	        System.out.println("XSD schema dumped to " + outputFilename);

		} catch ( Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		} 
    }
    
    public static OWLOntology shouldLoad(String fileName) throws OWLOntologyCreationException {
        // Get hold of an ontology manager
        manager = OWLManager.createOWLOntologyManager();
        //	Get the input file
        File file = new File(fileName);     
        //Load the ontology from the file
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
        //Check if the ontology contains any axioms     
        System.out.println("Number of axioms: " + ontology.getAxiomCount());
        System.out.println("Loaded ontology: " + ontology);
        // We can always obtain the location where an ontology was loaded from
        IRI documentIRI = manager.getOntologyDocumentIRI(ontology);
        System.out.println("    from: " + documentIRI);
        return ontology;
    }

	public static String suffixChange(String filename, String extension){
		String name = filename;
		int index = filename.lastIndexOf(".");
		if ( index != -1 ) {
			name = filename.substring(0, index);
			name += ".";
			name += extension;
		}
		return name;
	}

    
}
