package desiree.org.owl2xsd;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;

public class XSDDumper {
	private static 	String doubleQuote = "\"";
	private static 	String tab = "\t";	
	
	public XSDDumper( ){
		
	}

	public void dump(OWLOntology o, PrintWriter writer){
		IRI onto_iri =o.getOntologyID().getOntologyIRI();
		String ontoName = onto_iri.toString();
		String moduleName = onto_iri.getFragment();

		//	Header
		writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.println("<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" 	attributeFormDefault=\"unqualified\">");
		//	Classes
		class OWLClassComparator implements Comparator<OWLClass> {
            public int compare(OWLClass e1, OWLClass e2) {
                return e1.getIRI().compareTo(e2.getIRI());
            }
        }
				
		//	Get a sorted list of our classes
		Set<OWLClass> unsorted =  o.getClassesInSignature(false);
		ArrayList<OWLClass> sortedClasses = new ArrayList<OWLClass>(unsorted);
		Collections.sort(sortedClasses , new OWLClassComparator());		
		//	 Collect all (name,type) pairs for the class
		

		for (OWLClass cls : sortedClasses) {
		    HashMap<String, String> dataProperties = new HashMap<String, String>();
		    HashMap<String, String> objectProperties = new HashMap<String, String>();
		    HashMap<String, String> boundedProperties = new HashMap<String, String>();

			//	Get the annotations on the class that use the label property
			//	Only for the current ontology do not check imported classes
			if ( ! isExternalEntity(o, cls) ){
				//	Class declaration
				String className = cls.getIRI().getFragment();
				writer.println("<xs:complexType name=" + doubleQuote + className + doubleQuote + ">");
				//	grep the properties associated wit that class
				for ( OWLAxiom ax : o.getReferencingAxioms(cls)){
					//	Dataproperties 
					if ( ax instanceof OWLDataPropertyDomainAxiom ){
						OWLDataPropertyDomainAxiom axDomain = (OWLDataPropertyDomainAxiom)ax;
						OWLClassExpression expr = axDomain.getDomain();
						OWLDataPropertyExpression prop = axDomain.getProperty();
						//	If the class is the domain of some dataproperty
						if ( expr.asOWLClass().equals(cls)) {
							//	Get this property
							OWLDataProperty dataProp = prop.asOWLDataProperty();
							String propName = dataProp.getIRI().getFragment();
							// Find out its range 
							for ( OWLAxiom axiom : o.getReferencingAxioms(dataProp)){
								if ( axiom instanceof OWLDataPropertyRangeAxiom ){
									OWLDataPropertyRangeAxiom propRange = (OWLDataPropertyRangeAxiom)axiom;
									OWLDataRange dataRange = propRange.getRange();
									//	And its datatype
									if ( dataRange.isDatatype()){
									    OWLDatatype type = dataRange.asOWLDatatype();
									    String propType = type.toString();
									    propType = propType.replace("XMLSchema", "xs");
									    propType = propType.replace("xsd:", "xs:");
									    dataProperties.put(propName, propType);
									}
								}
							}
						}
					} else if ( ax instanceof OWLObjectPropertyDomainAxiom ){
						//	And object properties
						OWLObjectPropertyDomainAxiom axDomain = (OWLObjectPropertyDomainAxiom)ax;
						OWLClassExpression expr = axDomain.getDomain();
						OWLObjectPropertyExpression prop = axDomain.getProperty();
					
						if ( expr.asOWLClass().equals(cls) ) {
							//	Collect the property
							OWLObjectProperty objectProp = prop.asOWLObjectProperty();
							
							String propName = objectProp.getIRI().getFragment();
							// get attributes, functionnal or not
							boolean functional = objectProp.isFunctional(o);
							String propBounding = functional ? "1" : "unbounded";

							// Find out its range
							for ( OWLAxiom axiom : o.getReferencingAxioms(objectProp)){
								if ( axiom instanceof OWLObjectPropertyRangeAxiom ){
									OWLObjectPropertyRangeAxiom propRange = (OWLObjectPropertyRangeAxiom)axiom;
									OWLClassExpression classRange = propRange.getRange();
									if ( ! classRange.isAnonymous()){
										OWLClass propClass = classRange.asOWLClass();
										String propType = propClass.getIRI().getFragment();
										
										objectProperties.put(propName, propType);
										boundedProperties.put(propName, propBounding);
									}
								}
							}
						}
					}
				}
				OWLClass parent = getSuperClass( o,cls );
				if ( parent != null ){
					//	has parent 
					String parentName = parent.getIRI().getFragment();
					writer.println(tab+"<xs:complexContent>");
					int propertyNum = dataProperties.size() + objectProperties.size() ;
					if ( propertyNum >= 1 ){
						writer.println(tab+tab+"<xs:extension base=" + doubleQuote + parentName + doubleQuote + ">");
						buildPropSequence( dataProperties, objectProperties,boundedProperties, writer);	
						writer.println(tab+tab+"</xs:extension>");
					} else {
						writer.println(tab+tab+"<xs:extension base=" + doubleQuote + parentName + doubleQuote + "/>");
					}
					writer.println(tab+"</xs:complexContent>");
				} else {
					buildPropSequence( dataProperties, objectProperties, boundedProperties, writer);
				}
				writer.println("</xs:complexType>");
			}
		}
		writer.println("</xs:schema>");
		writer.flush();
	}

	private void buildPropSequence( HashMap<String, String> dataProperties, HashMap<String, String> objectProperties, HashMap<String, String> boundedProperties,PrintWriter writer){
		if ( (dataProperties.size() + objectProperties.size() ) > 0 ){
			writer.println(tab+"<xs:sequence>");
			if ( dataProperties.size() > 0 ){
				buildDataPropSequence(dataProperties, writer);
			}
			if ( objectProperties.size() > 0 ){
				buildObjectPropSequence(objectProperties, boundedProperties, writer);
			}
			writer.println(tab+"</xs:sequence>");
		}
	}
	
	private void buildObjectPropSequence( HashMap<String, String> objectProperties,HashMap<String, String> boundedProperties,PrintWriter writer){

		for ( String name : objectProperties.keySet() ){
			String type = objectProperties.get(name);
			// unbounded only if not functional
			String bounding = boundedProperties.get(name);
			writer.println(tab + tab+ "<xs:element name=" + doubleQuote + name + doubleQuote +
										" type=" + doubleQuote + type + doubleQuote + 
										" maxOccurs=" + doubleQuote + bounding + doubleQuote + "/>");
		}		
	}
	
	private void buildDataPropSequence( HashMap<String, String> dataProperties, PrintWriter writer){
		String doubleQuote = "\"";

		for ( String name : dataProperties.keySet() ){
			String type = dataProperties.get(name);
			writer.println(tab+tab+"<xs:element name=" + doubleQuote + name + doubleQuote + " type=" + doubleQuote + type + doubleQuote + "/>");
		}
	}

	public static boolean isExternalEntity(OWLOntology o, OWLEntity ent){
		String entPrefix = ent.getIRI().getNamespace();
		if ( entPrefix.toString().contains("#") && ! entPrefix.endsWith("#") ){
			//	Patch for getNameSpace
			//	we do not want things like http://openfoodsystem.org/ontology/personne#25
			int index =  entPrefix.toString().indexOf("#");
			entPrefix = entPrefix.substring(0, index + 1);
		}
		
		IRI onto_iri =o.getOntologyID().getOntologyIRI();
		String ontoName = "";
		if ( onto_iri.toString().endsWith("#")){
			ontoName = onto_iri.toString();
		} else {
			ontoName = onto_iri.toString() + "#";
		}
		if ( o.equals(ent) ){
			return false;			
		}
		if ( ontoName.equals(entPrefix) ) {
			return false;
		}
		return true;
	}
	
	public OWLClass getSuperClass( OWLOntology o, OWLClass cls ) {
		Set<OWLClassExpression> supers = cls.getSuperClasses(o);
		if ( ! supers.isEmpty()){
			OWLClassExpression parentExpr = supers.iterator().next();
			if ( ! parentExpr.isOWLNothing() && parentExpr.isClassExpressionLiteral() ) {
				return parentExpr.asOWLClass();
			}
		}
		return null;
	}

}
